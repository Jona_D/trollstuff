package us.jonaAndZach.trollStuff.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import us.jonaAndZach.trollStuff.TSMain;
import us.jonaAndZach.trollStuff.events.EntityDamageListener;

/**
 * Created by Owen on 27/10/2015.
 */
public class CommandSpinhit implements CommandExecutor {
	private TSMain plugin;
	public CommandSpinhit(TSMain instance){
		plugin = instance;
	}
	
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!sender.hasPermission("trollstuff.spinhit")) {return true;}
        final Player p = (Player) sender;
        String name = p.getName();

        if(args.length != 0) {
            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Invalid Syntax. " + ChatColor.RED + "/spinhit");
            return true;
        }

        if(args.length == 0) {
            if(EntityDamageListener.hasDahPower.contains(name)){
                EntityDamageListener.hasDahPower.remove(name);
                p.sendMessage(ChatColor.GREEN + "You unfortunately no longer have the power!");
                p.playSound(p.getLocation(), Sound.WOOD_CLICK, 1.5F, 0.5F);

                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.25F);
                        p.getWorld().playEffect(p.getLocation(), Effect.FIREWORKS_SPARK, 5);
                    }
                }, 10L);

                return true;
            } else {
                EntityDamageListener.hasDahPower.add(name);
                p.sendMessage(ChatColor.GREEN + "You now, indeed have the power!");
                p.playSound(p.getLocation(), Sound.WOOD_CLICK, 1.5F, 0.5F);

                return true;
            }
        }

        return false;
    }
}
