package us.jonaAndZach.trollStuff;

import org.bukkit.plugin.java.JavaPlugin;

import org.fusesource.jansi.Ansi;
import us.jonaAndZach.trollStuff.commands.CommandSpinhit;
import us.jonaAndZach.trollStuff.events.EntityDamageListener;
import us.jonaAndZach.trollStuff.events.SneakEventListener;

public class TSMain extends JavaPlugin {

	public void onEnable(){
		getLogger().info(Ansi.ansi().fg(Ansi.Color.GREEN).boldOff().toString() +" -- -- Starting TrollStuff v1.0.0 by Jona && Zach -- -- "+ Ansi.ansi().fg(Ansi.Color.WHITE).boldOff().toString());

		getServer().getPluginManager().registerEvents(new SneakEventListener(), this);
		getServer().getPluginManager().registerEvents(new EntityDamageListener(this), this);

		getLogger().info(" -- -- Initializing Commands... ]");
		initializeCommands();
		getLogger().info(" -- -- Initialized Commands Successfully! ]");
		getLogger().info(Ansi.ansi().fg(Ansi.Color.GREEN).boldOff().toString() +" -- -- Started TrollStuff v1.0.0 Successfully -- -- ]" + Ansi.ansi().fg(Ansi.Color.WHITE).boldOff().toString());

	}

	private void initializeCommands() {
		getCommand("spinhit").setExecutor(new CommandSpinhit(this));
	}


}
