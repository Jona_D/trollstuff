package us.jonaAndZach.trollStuff.events;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

import us.jonaAndZach.trollStuff.particles.ParticleEffect;

public class SneakEventListener implements Listener {

	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e){
		if (e.isSneaking()){
			if (e.getPlayer().hasPermission("trollstuff.fart")){
				float yaw = 180 - e.getPlayer().getLocation().getYaw();
				/*boolean positiveX = yaw < 0 && yaw > -180;
			boolean positiveZ = yaw < 90 && yaw > -90;
			boolean upward = pitch < 0;*/

				double overstaand = Math.sin(Math.toRadians(yaw));
				double aanliggend = Math.cos(Math.toRadians(yaw));

				Vector vector = new Vector(overstaand, 0.5, aanliggend);
				System.out.println(overstaand + ", " + aanliggend);
				Location loc = e.getPlayer().getLocation();
				loc.setY(loc.getY() + 0.75);
				ParticleEffect.SNOW_SHOVEL.display(vector, 0.3F, loc, Float.MAX_VALUE);
				ParticleEffect.SMOKE_NORMAL.display(vector, 0.3F, loc, Float.MAX_VALUE);
				for (int i = 0; i < 5; i++){
					Random rand = new Random();
					double check = rand.nextDouble();
					double neg = rand.nextDouble();
					if (neg > 0.50){
						check = -check;
					}
					Vector vec2 = vector.clone();
					vec2.setX(vec2.getX()*(check*0.1));
					vec2.setY(vec2.getY()*(check*0.1));
					vec2.setZ(vec2.getZ()*(check*0.1));
					ParticleEffect.SNOW_SHOVEL.display(vec2, 0.3F, loc, Float.MAX_VALUE);
					ParticleEffect.SMOKE_NORMAL.display(vec2, 0.3F, loc, Float.MAX_VALUE);
				}

				System.out.println("Played effect");

			}
		}
	}
}
