package us.jonaAndZach.trollStuff.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import us.jonaAndZach.trollStuff.TSMain;

public class EntityDamageListener implements Listener {

	private TSMain main;
	private HashMap<UUID, Integer> taskList = new HashMap<UUID, Integer>();

	public EntityDamageListener(TSMain instance) {
		this.main = instance;
	}

	public static List<String> hasDahPower = new ArrayList<String>();

	boolean spawnParticles = true;
	boolean playSound = true;

	//boolean fusRodah = false;
	boolean forceParticles = false;
	boolean denyParticles = false;

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamageEntity(EntityDamageByEntityEvent e) {
		Entity at = e.getDamager();
		Entity ate = e.getEntity();

		if(!hasDahPower.contains(at.getName())){ return; }
		//fusRodah = true;

		double damage = e.getDamage();
		final double bonusdmg = damage * 1.5;
		e.setDamage(bonusdmg);

		if(ate instanceof Player) {
			Location loc1 = ate.getLocation();
			pushAwayPlayer(at,((Player)ate), 3.0);
			Location loc2 = ate.getLocation(); //Maybeh, use a runnable??
			playSpecialParticles(ate, loc1, loc2);
			ate.getWorld().playSound(ate.getLocation(), Sound.EXPLODE, 1F, 0.3F); // Ouch!
			spin((Player)ate);

			//ate.playEffect(EntityEffect.FIREWORK_EXPLODE);
			ate.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + at.getName() + ": " + ChatColor.WHITE
					+ "FUUUUUUUUUUSRODAAAAAAAAAAAAAHHHH!!!!!!!!");
		} else {
			Location loc1 = ate.getLocation();
			pushAwayEntity(((Player) at), ate, 3.0);
			Location loc2 = ate.getLocation(); //Maybeh use a runnable??
			playSpecialParticles(ate, loc1, loc2);
			ate.getWorld().playSound(ate.getLocation(), Sound.EXPLODE, 1F, 0.3F); // Ouch!



		}

	}

	public void spin(final Player player){
		final int task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
			int spin = 30;
			boolean canSpin = true;
			public void run() {

				if(!canSpin) {
					Bukkit.getScheduler().cancelTask(taskList.get(player.getUniqueId()));
				}

				if((canSpin)) {
                    Location loc = player.getLocation();
					String facing[] = {"W", "NW", "N", "NE", "E", "SE", "S", "SW"};
					double yaw = ((loc.getYaw() + 22.5) % 360);
					if (yaw < 0) yaw = 360 + yaw + 55;
					loc.setYaw((float) yaw);
					player.teleport(loc);
					player.sendMessage("Facing " + facing[(int) (yaw / 45)]);
					spin = spin - 1;
				}

				if(spin >= 0) {
					canSpin = false;
				}
			}
		}, 10L, 2);
		taskList.put(player.getUniqueId(), task);
	}

	public void playSpecialParticles(Entity p, Location from, final Location to) {

		Location l = p.getLocation();
		l.setY(400);

		// playSound = (!denySound) && ((forceSound) || (playSound));
		if(p instanceof Player) {
			Player player = (Player) p;
			player.playSound(p.getLocation(), Sound.WITHER_SHOOT, 1.0F, -0.0F);
		}

		spawnParticles = (!denyParticles)
				&& ((forceParticles) || (spawnParticles));
		if (playSound) {
			if (from.getWorld().equals(to.getWorld())) {
				if (from.distanceSquared(to) >= 2) {
					from.getWorld().playSound(from,
							Sound.ENDERMAN_TELEPORT, 0.3F, 1.0F);

					Bukkit
					.getServer()
					.getScheduler()
					.scheduleSyncDelayedTask(main,
							new Runnable() {
						public void run() {
							to.getWorld()
							.playSound(
									to,
									Sound.ENDERMAN_TELEPORT,
									0.3F, 1.0F);
						}
					}, from.distance(to) >= 5.0D ? 1L : 0L);
				}
			} else {
				from.getWorld().playSound(from, Sound.ENDERMAN_TELEPORT,
						0.3F, 1.0F);

				Bukkit
				.getServer()
				.getScheduler()
				.scheduleSyncDelayedTask(main,
						new Runnable() {
					public void run() {
						to.getWorld().playSound(to,
								Sound.ENDERMAN_TELEPORT,
								0.3F, 1.0F);
					}
				}, 1L);
			}
		}
		if (spawnParticles) {
			if ((!from.getWorld().equals(to.getWorld()))
					|| (from.distanceSquared(to) >= 2)) {
				spawnSmoke(from, 5.0F);
				spawnSmoke(from.add(0.0D, 1.0D, 0.0D), 5.0F);
				from.getWorld().playEffect(from, Effect.MOBSPAWNER_FLAMES,
						null);
				Bukkit
				.getServer()
				.getScheduler()
				.scheduleSyncDelayedTask(main, new Runnable() {
					public void run() {
						for (int i = 0; i < 3; i++) {
							to.getWorld().playEffect(to,
									Effect.ENDER_SIGNAL,
									null);
							to.getWorld().playEffect(to,
									Effect.ENDER_SIGNAL,
									null);
							to.add(0.0D, 1.0D, 0.0D);
						}
					}
				}, 1L);
			}
		}

	}

	public void pushAwayEntity(Player p, Entity entity, double speed) {
		// Get velocity unit vector:
		org.bukkit.util.Vector unitVector = entity.getLocation().toVector().subtract(p.getLocation().toVector()).normalize();
		// Set speed and push entity:
		entity.setVelocity(unitVector.multiply(speed));
	}

	public static void pushAwayPlayer(Entity entity, Player p, double speed) {
		// Get velocity unit vector:
		org.bukkit.util.Vector unitVector = p.getLocation().toVector().subtract(entity.getLocation().toVector()).normalize();
		// Set speed and push entity:
		double e_y = entity.getLocation().getY();
		double p_y = p.getLocation().getY();

		Material m = p.getLocation().subtract(0, 1, 0).getBlock().getType();

		if ((p_y - 1) <= e_y || m == Material.AIR) {
			p.setVelocity(unitVector.multiply(speed));
		}
	}

	public static Random random = new Random();

	public static void spawnSmoke(Location location, float thickness) {
		int singles = (int) Math.floor(thickness * 9.0F);
		for (int i = 0; i < singles; i++) {
			if (location == null) {
				return;
			}
			location.getWorld().playEffect(location, Effect.SMOKE,
					random.nextInt(9));
		}
	}
}
